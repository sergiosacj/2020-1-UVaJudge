# Entrevista

## 1. Introdução

<p align="justify"> &emsp;&emsp; Trata-se de uma conversa guiada por um roteiro de perguntas e tópicos, nos quais um entrevistador busca obter informações de um entrevistado. </p>

<p align="justify"> &emsp;&emsp; ​Ao conduzirmos entrevistas com diversos perfis de usuário sobre o mesmo sistema, obtemos uma visão profunda e abrangente dos tópicos investigados. </p>

<p align="justify"> &emsp;&emsp; Infelizmente, por se tratar de tempos de pandemia, nosso acesso à usuários foi por meio de vídeo chamadas. Assim, conseguimos entrevistar apenas aqueles que tinhamos contato prévio. </p>

## 2. Entrevista

### 2.1 Entrevista com Jacinto José

#### 1. Qual o seu nome?

Jacinto josé.

#### 2. Quantos anos você tem?

25 anos.

#### 3. Qual a sua profissão/ocupação?

Desenvolvedor frontend.

#### 4. Você já participou em uma maratona de programação?

Não.

#### 5. Você já utilizou algum site de programação para resolver questões? 

Sim, mas não utilizei por muito tempo.

#### 6. Você conhece o site UVA? Se sim, por onde conheceu?

Sim, precisava da solução de uma questão de quando eu fazia estrutura de dados.

#### 7. Qual foi a sua primeira impressão do UVA?

Péssima, ele não tem uma usabilidade muito boa.

#### 8. Você ainda usa o UVA?

Não, e espero nunca mais usar.


### 2.2 Entrevista Ana Maria

#### 1. Qual o seu nome?

Ana maria.

#### 2. Quantos anos você tem?

19 anos.

#### 3. Qual a sua profissão/ocupação?

Estudante de engenharia de software.

#### 4. Você já participou em uma maratona de programação?

Sim.

#### 5. Você já utilizou algum site de programação para resolver questões? 

Sim, utilizo constantemente.

#### 6. Você conhece o site UVA? Se sim, por onde conheceu?

Sim, infelizmente tive que usar para práticar algumas questões. Eu conheci pesquisando questões de ordenação pelo google mesmo.

#### 7. Qual foi a sua primeira impressão do UVA?

Não foi muito boa.

#### 8. Você ainda usa o UVA?

Ás vezes.

### 2.3 Entrevista Maurício de José

#### 1. Qual o seu nome?

Maurício de José.

#### 2. Quantos anos você tem?

29 anos.

#### 3. Qual a sua profissão/ocupação?

Estudante, fazendo curso de matemática.

#### 4. Você já participou em uma maratona de programação?

Não.

#### 5. Você já utilizou algum site de programação para resolver questões? 

Sim.

#### 6. Você conhece o site UVA? Se sim, por onde conheceu?

Sim. Pesquisando uma questão no google para aplicar os meus conhecimentos em matemática.

#### 7. Qual foi a sua primeira impressão do UVA?

Tive uma grande dificuldade para conseguir acessar as questões.

#### 8. Você ainda usa o UVA?

Não.



## 2. Versionamento

|Data|Versão|Descrição|Autor|
|:-:|:-:|:-:|:-:|
|02/10/2020|1.0|Criação das 3 entrevistas e introdução|Paulo Batista|
|04/10/2020|1.1|Correções na formatação|Sérgio Cipriano|
