# Apresentação 2

## 1. Introdução

<p align="justify"> &emsp;&emsp; Devido ao isolamento social, a apresentação será por meio de vídeo. O grupo se organizou para realizar a gravação da apresentação e edição do video gravado. Utilizamos o teams para gravarmos a apresentação e para editar o vídeo utilizamos a ferramenta do site <a href = "https://www.onlineconverter.com/merge-video"> onlineconverter.com </a>.</p>

## 2. Progresso

<p align="justify"> &emsp;&emsp; O grupo produziu vários documentos entre a apresentação 1 e a gravação da apresentação 2. Segue abaixo lista dos feitos nesse período:</p>

* Análise do usuário;
* Análise de tarefas;
* Perfil de usuário;
* Personas;
* Resultados do questionário (google forms);
* Atas de reunião: 5-8;
* Correção dos materiais da primeira apresentação.

## 3. Apresentação

<iframe width="720" height="480" src="https://www.youtube-nocookie.com/embed/lYM5pzatzz4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 4. Versionamento

|Data|Versão|Descrição|Autor|
|:-:|:-:|:-:|:-:|
|04/09/2020|1.0|Gravação da apresentação|Sérgio Cipriano, Washington Bispo, Paulo Batista|
|04/09/2020|1.1|Edição do vídeo e criação do página no github pages|Washington Bispo|
|05/09/2020|1.2|Adição de tarefa concluida|Sérgio Cipriano|

