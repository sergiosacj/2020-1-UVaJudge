# Apresentação 1

## 1. Introdução

<p align="justify"> &emsp;&emsp; Devido ao isolamento social, a apresentação será por meio de vídeo. O grupo se organizou para realizar a gravação da apresentação e edição do video gravado. Utilizamos o teams para gravarmos a apresentação e para editar o vídeo utilizamos a ferramenta do site <a href = "https://www.onlineconverter.com/merge-video"> onlineconverter.com </a>.</p>

## 2. Progresso

<p align="justify"> &emsp;&emsp; O grupo produziu vários documentos entre o início do projeto e a gravação da apresentação. Segue abaixo lista de documentos feitos nesse período:</p>

* Todos os 3 planejamentos individuais;
* Todas as 3 avaliações individuais;
* Cronograma de atividades;
* Ferramentas utilizadas no projeto;
* Porque escolhemos o UVa;
* Processo de design;
* Atas de reunião: 1-4;
* Aba Sobre/README do projeto.


## 3. Apresentação

<iframe width="720" height="480" src="https://www.youtube-nocookie.com/embed/3KLGlM6r0eE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 4. Versionamento

|Data|Versão|Descrição|Autor|
|:-:|:-:|:-:|:-:|
|19/09/2020|1.0|Gravação da apresentação|Sérgio Cipriano, Washington Bispo, Paulo Batista|
|19/09/2020|1.1|Edição do vídeo|Washington Bispo|
|20/09/2020|1.2|Criação do página no github pages|Sérgio Cipriano|
|20/09/2020|1.3|Adição do vídeo|Washington Bispo|